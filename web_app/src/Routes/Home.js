import {Container} from "react-bootstrap";
import React,{useState} from "react";

function Home() {
    const[cas, setCas]=useState();

    return (
        <Container>
            <h1>Welcome</h1>
            
            <input type="radio" name="cas" value="Trenutno je dan" onChange={e=>setCas(e.target.value)}/>Dan
            <input type="radio" name="cas" value="Trenutno je noc" onChange={e=>setCas(e.target.value)}/>Noc

            <h1 data-testid="cas">{cas}</h1>

            <button data-testid="gumb" value="" onClick={e=>setCas(e.target.value)}>Click Me</button>

            

        </Container>
    );
}

export default Home;
