import Home from '../Routes/Home'
import {render, fireEvent, screen} from "@testing-library/react";

test('test_dan_clicked_passed', () => {
    render(<Home />);
    const radio1 = screen.getByDisplayValue("Trenutno je dan");
    fireEvent.click(radio1);
    expect(radio1).toBeChecked();
});

test('test_noc_clicked_passed', () => {
    render(<Home />);
    const radio2 = screen.getByDisplayValue("Trenutno je noc");
    fireEvent.click(radio2);
    expect(radio2).toBeChecked();
});

test('test_dan_text_displayed_passed', () => {
    render(<Home />);
    const radio1 = screen.getByDisplayValue("Trenutno je dan");
    const text = screen.getByTestId("cas");
    fireEvent.click(radio1);
    expect(radio1).toBeChecked();
    expect(text).toBeInTheDocument();
});

test('test_noc_text_displayed_passed', () => {
    render(<Home />);
    const radio2 = screen.getByDisplayValue("Trenutno je noc");
    const text = screen.getByTestId("cas");
    fireEvent.click(radio2);
    expect(radio2).toBeChecked();
    expect(text).toBeInTheDocument();
});

test('test_gumb_displayed_passed', () => {
    render(<Home />);
    const gumb = screen.getByTestId("gumb");
    expect(gumb).toBeInTheDocument();
});

test('test_gumb_clicked_text_notdisplayed_passed', () => {
    render(<Home />);
    const gumb = screen.getByTestId("gumb");
    fireEvent.click(gumb);
    const text = screen.getByTestId("cas");
    expect(gumb).toBeInTheDocument();
    expect(text.innerHTML).toMatch("");
});