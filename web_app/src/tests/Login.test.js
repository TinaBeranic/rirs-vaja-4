import Login from '../Routes/Login'
import {render, screen} from "@testing-library/react";

test('test_placeholder_login_passed', () => {
    render(<Login />);
    const uname = screen.getByPlaceholderText("Vnesi uporabniško ime");
    const password = screen.getByPlaceholderText("Vnesi geslo");
    expect(uname).toBeInTheDocument();
    expect(password).toBeInTheDocument();
});

test('test_button_onscreen_passed', () => {
    render(<Login />);
    const register = screen.getByRole('button');
    expect(register).toBeInTheDocument();
});
