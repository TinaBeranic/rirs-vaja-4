import Register from '../Routes/Register'
import {render, screen} from "@testing-library/react";

test('test_placeholder_register_passed', () => {
    render(<Register />);
    const uname = screen.getByPlaceholderText("Vnesi uporabniško ime");
    const password = screen.getByPlaceholderText("Vnesi geslo");
    const repeatPassword = screen.getByPlaceholderText("Ponovi geslo");
    expect(uname).toBeInTheDocument();
    expect(password).toBeInTheDocument();
    expect(repeatPassword).toBeInTheDocument();
});

test('test_button_onscreen_passed', () => {
    render(<Register />);
    const register = screen.getByRole('button');
    expect(register).toBeInTheDocument();
});
