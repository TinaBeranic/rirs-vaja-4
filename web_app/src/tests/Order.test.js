import Order from '../Routes/Order'
import {render, fireEvent, screen} from "@testing-library/react";

test("test_radio1_checked_passed", () => {
    render(<Order/>);
    const radio1 = screen.getByLabelText("Plačilo preko spleta");
    fireEvent.click(radio1);
    expect(radio1).toBeChecked();
});

test("test_radio2_checked_passed", () => {
    render(<Order/>);
    const radio2 = screen.getByLabelText("Plačilo po povzetju");
    fireEvent.click(radio2);
    expect(radio2).toBeChecked();
});

test("test_radio2_not_checked_passed", () => {
    render(<Order/>);
    const radio1 = screen.getByLabelText("Plačilo preko spleta");
    const radio2 = screen.getByLabelText("Plačilo po povzetju");
    fireEvent.click(radio1);
    expect(radio2).not.toBeChecked();
});

test("test_radio1_not_checked_passed", () => {
    render(<Order/>);
    const radio1 = screen.getByLabelText("Plačilo preko spleta");
    const radio2 = screen.getByLabelText("Plačilo po povzetju");
    fireEvent.click(radio2);
    expect(radio1).not.toBeChecked();
});

test('test_radio1_placeholder_placiloPrekoSpleta_passed', () => {
    render(<Order/>);
    const radio1 = screen.getByLabelText("Plačilo preko spleta");
    fireEvent.click(radio1);
    const name = screen.getByPlaceholderText("Vnesi ime na kartici");
    const card = screen.getByPlaceholderText("Vnesi številko kartice");
    const cvv = screen.getByPlaceholderText("Vnesi številko cvv");
    expect(name).toBeInTheDocument();
    expect(card).toBeInTheDocument();
    expect(cvv).toBeInTheDocument();
});

test('test_radio2_placeholder_placiloPoPovzetju_passed', () => {
    render(<Order/>);
    const radio2 = screen.getByLabelText("Plačilo po povzetju");
    fireEvent.click(radio2);
    const fname = screen.getByPlaceholderText("Vnesi ime");
    const lcard = screen.getByPlaceholderText("Vnesi priimek");
    const address = screen.getByPlaceholderText("Vnesi naslov");
    const city = screen.getByPlaceholderText("Vnesi mesto");
    const postNum = screen.getByPlaceholderText("Vnesi poštno številko");
    expect(fname).toBeInTheDocument();
    expect(lcard).toBeInTheDocument();
    expect(address).toBeInTheDocument();
    expect(city).toBeInTheDocument();
    expect(postNum).toBeInTheDocument();
});


