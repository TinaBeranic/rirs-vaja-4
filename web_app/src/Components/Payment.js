import {Alert, Button, Form} from "react-bootstrap";
import {useState} from "react";

function Payment() {
    const [name, setName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("");
    const [postalCose, setPostalCode] = useState("");
    const [error] = useState("");
    const [isError] = useState(false);
    const [cardName, setCardName] = useState("")
    const [cardNumber, setCardNumber] = useState("")
    const [cvv, setCvv] = useState("")
    const [type, setType] = useState("povzetje")
    let cardPayment
    let deliveryPayment

    const testSubmit = e => {
        console.log("submit")
    }

    cardPayment =
        <Form className={"mt-4"} data-testid="spletni_form" onSubmit={testSubmit}>
            <Form.Group className="mb-3" controlId="formUsername">
                <Form.Label>Ime</Form.Label>
                <Form.Control type="text" placeholder="Vnesi ime na kartici" id={"username"} value={name}
                              onChange={(e) => setName(e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Številka kartice</Form.Label>
                <Form.Control type="text" placeholder="Vnesi številko kartice" id={"password"} value={cardNumber}
                              onChange={(e) => setCardNumber(e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Cvv</Form.Label>
                <Form.Control type="text" placeholder="Vnesi številko cvv" id={"password"} value={cvv}
                              onChange={(e) => setCvv(e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Button type={"submit"} aria-label={"submit_online"}>Pošlji</Button>
            </Form.Group>
            {/*<Button onClick={sendLogin} className={"mb-3"} variant={"outline-primary"}>Prijava</Button>*/}
            {isError &&
                <Alert variant={"danger"}>
                    {error}
                </Alert>
            }
        </Form>

    deliveryPayment  =
        <Form className={"mt-4"}>
            <Form.Group className="mb-3" controlId="formUsername">
                <Form.Label>Ime</Form.Label>
                <Form.Control type="text" placeholder="Vnesi ime" id={"username"} value={cardName}
                              onChange={(e) => setCardName(e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Priimek</Form.Label>
                <Form.Control type="text" placeholder="Vnesi priimek" id={"password"} value={lastName}
                              onChange={(e) => setLastName(e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Naslov</Form.Label>
                <Form.Control type="text" placeholder="Vnesi naslov" id={"password"} value={address}
                              onChange={(e) => setAddress(e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Mesto</Form.Label>
                <Form.Control type="text" placeholder="Vnesi mesto" id={"password"} value={city}
                              onChange={(e) => setCity(e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Poštna številka</Form.Label>
                <Form.Control type="text" placeholder="Vnesi poštno številko" id={"password"} value={postalCose}
                              onChange={(e) => setPostalCode(e.target.value)}/>
            </Form.Group>
            {/*<Button onClick={sendLogin} className={"mb-3"} variant={"outline-primary"}>Prijava</Button>*/}
            {isError &&
                <Alert variant={"danger"}>
                    {error}
                </Alert>
            }
        </Form>


    const handleChange = (e) => {
        setType(e.target.id)
    }
    return (
        // <Container>
        <div>
            <p><b>Način plačila</b></p>
            <Form>
                <div key={`inline-radio`} className="mb-3">
                    <Form.Check
                        inline
                        label="Plačilo preko spleta"
                        name="group1"
                        type={"radio"}
                        id={`spletno`}
                        onChange={handleChange}
                        value={"spletno"}
                    />
                    <Form.Check
                        inline
                        label="Plačilo po povzetju"
                        name="group1"
                        type={"radio"}
                        id={`povzetje`}
                        value={"povzetje"}
                        onChange={handleChange}
                        defaultChecked
                    />
                </div>
            </Form>
            {/*{method}*/}
            {
                type === "spletno" &&
                (cardPayment)
            }
            {
                type === "povzetje" &&
                (deliveryPayment)
            }
            {/*{cardPayment}*/}
        </div>
        /*</Container>*/
    );
}

export default Payment;
