from pickle import APPEND
from pydoc import cli
from flask import Flask
from flask_cors import CORS
from pymongo import MongoClient

base_url = '/api/v1'

app = Flask(__name__)

client = MongoClient("mongodb+srv://test123:test123@cluster0.zy8ouhh.mongodb.net/?retryWrites=true&w=majority")
db = client.rirs

ItemsCollection = db.izdelek
OrdersCollection = db.narocilo
PaymentCollection = db.placilo
UsersCollection = db.user


CORS(app)

import routs

#run -> python -m flask --debug run